# Morse USB-Keyer

Projet permettant de réaliser un clavier morse pour ordinateur. Idéal pour apprendre le morse et s'entraîner à sa pratique !

## Présentation

Objectif : réaliser un keyer morse USB pour pouvoir écrire ce que l'on souhaite sur l'ordinateur (ou le smartphone) en morse.

Visionnez une courte démonstration sur Youtube en cliquant sur l'image ci-dessous :

[![Morse USB Keyer sur Youtube](doc/miniature.jpg)](https://www.youtube.com/watch?v=TX6meCOgaZw "Cliquez pour regarder sur Youtube")

Quand j'ai une idée qui me traverse l'esprit, le premier truc que je fais c'est que je vais sur internet et je regarde si certains ne l'aurait pas déjà fait. En général, il y a souvent des projets similaire à ce que vous voulez faire et je vous conseille de procéder de la sorte, cela vous permettra de voir comment les autres ont fait, comprendre et vous améliorer, progresser !  
Dans ce cas précis, il se trouve que Ralf Beesner (DK5BU) l'a déjà fait et même très bien ! Donc je ne me suis pas trop fatigué, je ne savais pas trop comment procéder pour mesurer les temps d'appui sur le keyer, de relachement et transformer tout ça en caractères d'ailleurs au début. J'ai donc utilisé son programme, auquel j'ai apporté plusieurs modifications :  
* Je l'ai traduit en français.
* Je l'ai adapté pour nos ordinateurs français (histoires de QWERTY/AZERTY que vous verrez plus bas mais aussi qu'à la base son programme est prévu pour émuler un clavier allemand).
* J'ai corrigé quelques problèmes que j'avais pu rencontrer avec son code.
* Je l'ai commenté et documenté.

Bonne lecture/expérimentation !

## Fabrication

### Liste du matériel nécessaire

* 1 Module Digispark
* 1 Keyer morse
* 1 Résistance de 220k (valeur indicative)
* 1 Condensateur céramique 0.1µF (valeur indicative)
* 1 Dipswitch deux canaux ou 2 cavalier et des pins (facultatif)
* 1 Haut-parleur ou buzzer (facultatif)
* 1 ou 2 borniers à vis (facultatif)

Vous pouvez trouver le module Digispark pour quelques euros sur le market-place de votre choix (liens direct : [Amazon](https://www.amazon.fr/s?ie=UTF8&field-keywords=digispark), [Aliexpress](https://fr.aliexpress.com/wholesale?SearchText=digispark), [Banggood](https://www.banggood.com/fr/search/digispark.html) ...).  
Si vous avez des Attiny85 qui trainent, allez faire un tour sur ce tuto pour le transformer en Digispark : [le tuto de F4GOH](https://hamprojects.wordpress.com/2018/12/27/digispark-microcontrollers-implementation/).

### Le schéma

![Schema](doc/schema.PNG)

Le condensateur C1 sert ici d'anti-rebonds hardware. En fonction de la qualité de votre keyer, il peut être indispensable. La résistance R1 était sur le schéma d'origine, je l'ai conservé. Les entrées vont être configurées en INPUT_PULLUP, il n'est donc pas nécessaire d'ajouter de résistances de pull-up.

**IMPORTANT :** L'entrée du keyer est reliée sur le pin D1, sur ce pin est aussi relié la LED intégrée au Digispark. Afin d'éviter que le programme ne détecte pas les appuis sur le keyer, il est IMPERATIF de retirer cette LED. Pour cela, il suffit de dessouder la LED ou sa résistance associé. Pour ma part je suis parti sur la seconde solution comme vous pouvez le voir sur la photo suivante :

![LED retirée](doc/led_enlevee.jpg)

Libre à vous de réaliser le circuit électronique comme bon vous semble !

### Exemple de réalisation

![Ma réalisation schéma](doc/ma_real_schema.jpg)

Personnellement, je suis parti sur une réalisation de type "prototype", c'est à dire sans prise de tête ! J'ai réalisé le montage sur une plaque à trous. J'ai choisis d'utiliser des cavaliers pour sélectionner la vitesse de frappe, le mode pour les espaces et activer ou non le retour audio et un bornier pour relier le keyer, un autre pour le haut-parleur.

![Ma réalisation avec le keyer](doc/ma_real_keyer.jpg)

Le keyer est ici imprimé en 3D avec le modèle gentiment envoyé par [Almisuifre](https://twitter.com/Almisuifre) (que je remercie). Vous pouvez sinon trouver bien d'autres modèles similaires sur [Thingiverse](https://www.thingiverse.com/) ou concevoir votre propre keyer !  
(Le condensateur anti-rebonds n'est pas visible sur ces photos parce qu'il a été ajouté après-coup. Il est situé de l'autre côté du circuit.)

## Configuration de l'IDE Arduino et téléversement

Vous êtes maintenant prêt à passer dans le vif du sujet, la programmation de notre gadget !

### Installation de l'IDE et des cartes Digispark

* Installez (si vous ne l'avez pas déjà fait) l'IDE Arduino (>1.6.5).
* Lancez l'IDE Arduino.
* Dans le menu, ouvrez "Fichier", "Préférences". Une fenêtre s'ouvre.
* Nous allons modifier la ligne "URL de gestionnaires de cartes supplémentaires". Ajoutez à la fin de ce qu'il y a déjà de présent une virgule en guise de séparateur et coller y l'URL suivante :  
`http://digistump.com/package_digistump_index.json`
* Validez par "OK", fermez l'IDE et relancez le.
* Dans le menu, ouvrez "Outils", "Type de carte", "Gestionnaire de carte". Une fenêtre s'ouvre.
* Vérifiez que vous êtes bien en "Type : tout" en haut à gauche.
* Recherchez le paquet "Digistump AVR Boards" (vous pouvez vous aider de l'outil de recherche en haut à droite).
* Cliquez dessus, puis "Installer".
* Une fois l'installation complète, vous pourrez fermer cette fenêtre.

### Téléversement d'un programme d'exemple

* Ouvrez l'IDE Arduino.
* Dans le menu, "Outils", "Type de carte", choisissez "Digispark (Default - 16.5MHz)"  
Nous n'avons pas besoin de nous soucier du port série avec ce type de carte donc vous pouvez le laisser tel qu'il est.
* Ouvrez le programme d'exemple : Dans le menu, ouvrez "Fichier", "Exemples", "DigisparkKeyboard", "Keyboard".
* Si votre Digispark est branché à l'ordinateur, débranchez le.
* Vous pouvez maintenant lancer le téléversement en cliquant sur la flèche en haut à gauche (ou Ctrl + U).
* Le compilateur va vous inviter à brancher le Digispark, branchez le Digispark.
* Si vous obtenez "Micronucleus done. Thank you!", le téléversement est réussi et le programme va s'exécuter sur le Digispark.  
Sinon, si vous obtener une erreur 32, débranchez le Digispark et réessayer de téléverser le programme. Si le problème persiste, essayez avec un autre port USB.

A ce stade, le Digispark devrait se comporter comme un clavier aux yeux de votre ordinateur et devrait écrire une phrase toute les 5 secondes. Deux possibilités, vous obtenez à l'écran :

* "Hello Digispqrk1" : Votre ordinateur reconnait le Digispark comme un clavier QWERTY (pas de panique, c'est le cas sur plus de 90% des ordinateurs).
* "Hello Digispark!" : Votre ordinateur reconnait le Digispark comme un clavier AZERTY.

Gardez en tête de quelle manière votre Digispark est reconnu, on en aura besoin dans quelques instants.

### Téléversement du programme

* Récupérez le programme depuis le git. Vous avez 2 solutions, soit vous téléchargez le .zip du git depuis cette page et le décompressez sur votre ordinateur, soit vous clonez le répertoire :  
```
git clone https://framagit.org/Electroalex26/morse-usb-keyer.git
```
* La procédure est la même que pour le programme d'exemple. La seule différence est qu'il faut ouvrir le programme et non l'exemple donc "Fichier", "Ouvrir".  
* Si (et seulement si) votre Digispark est reconnu comme clavier AZERTY, commentez la ligne "#define QWERTY" en ajoutant "//" en début de ligne et décommentez la ligne "#define AZERTY" (en supprimant les "//")
* Vous pouvez si vous le désirez modifier les constantes de temps (situées au tout début de la fonction loop()) en fonction de votre vitesse d'écriture en morse. Par défaut les constantes sont réglées pour 60 et 90 caractères/minute.
* Téléversez le programme (cf téléversement du programme d'exemple)

Bravo, (normalement) votre keyer USB fonctionne parfaitement !

## Explication du code

Je ne vais pas rentrer ici trop en détails dans la structure du code, je vous invite à aller voir le code et lire les commentaires pour comprendre comment il fonctionne. Je vais ici me contenter d'expliquer la manière dont est encodé la variable `charactercode`.  
Cette variable est un octet, c'est à dire 8 bits. Par défaut, elle est initialisée à 1, soit en binaire : `0b 000 0001`. Ce "1" permet de définir le début de notre caractère, tous les bits qui le suivent correspondront donc à des dit ou des dah morse. Le programme va décaler tous les bits d'un rang vers la gauche et ajouter un 0 à l'emplacement du bit de poid faible (LSB, Least Significant Bit). Voici un exemple :

![Illustration décalage bit](doc/decalage_bit.jpg)

Ensuite en fonction des différentes durées qu'il a mesuré juste avant, il sait si l'utilisateur a entré un dit ou un dah. S'il a entré un dah, il ajoute 1 à la variable. Le LSB devient donc un 1. S'il s'agit d'un dit, le LSB reste 0.  
En regardant donc cette variable en binaire on peut réussir à déterminer un caractère morse. Exemple : `0b 0011 0110` va correspondre à "dah dit dah dah dit" (le premier 1 étant le séparateur par défaut), soit `-.--.`.  
On obtient donc une variable qui permet de stocker tous les caractères morse (référencés ou non d'ailleurs !). Il suffit donc de transformer cette valeur en un caractère ASCII (c'est le role du tableau `morsetable`) et le tour est joué ! Exemple : `0b 0011 0110` va être transformé en un `CR` (carriage return, retour à la ligne).  
Libre à vous d'ajouter vos propre caractères morse personnalisés dans ce tableau !

## Des questions ?

Me contacter ou me retrouver sur Twitter : [@Electroalex26](https://twitter.com/Electroalex26).  
Sinon me contacter via Framagit (mail sur le profil).

## Sources/Liens

* La réalisation de Ralf Beesner, DK5BU : [http://www.elektronik-labor.de/Arduino/Digispark-Morsekeyboard.html](http://www.elektronik-labor.de/Arduino/Digispark-Morsekeyboard.html)
* Faire un Digispark avec un Attiny85 : [https://hamprojects.wordpress.com/2018/12/27/digispark-microcontrollers-implementation/](https://hamprojects.wordpress.com/2018/12/27/digispark-microcontrollers-implementation/)
* IDE Arduino : [https://www.arduino.cc/en/Main/Software](https://www.arduino.cc/en/Main/Software)
* Digistump : [http://digistump.com/wiki/digispark/tutorials/connecting](http://digistump.com/wiki/digispark/tutorials/connecting)
* Code morse : [https://fr.wikipedia.org/wiki/Code_Morse_international](https://fr.wikipedia.org/wiki/Code_Morse_international)
* Démonstration :  
    * [https://twitter.com/Electroalex26/status/1049339318731464705](https://twitter.com/Electroalex26/status/1049339318731464705)
    * [https://www.youtube.com/watch?v=TX6meCOgaZw](https://www.youtube.com/watch?v=TX6meCOgaZw)
