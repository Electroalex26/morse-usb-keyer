/*############################################################################################################################
  Morse USB-Keyer

  Programme dérivé de celui de Ralf Beesner (DK5BU), lui même dérivé des travaux de Burkhard Kainka.
  http://www.elektronik-labor.de/Arduino/Digispark-Morsekeyboard.html

  Vous trouverez toutes les informations concernant ce programme, sa mise en pratique et son fonctionnement sur son git :
  https://framagit.org/Electroalex26/morse-usb-keyer

  Pour toutes questions, vous pouvez me contacter via Twitter à l'adresse suivante :
  https://twitter.com/Electroalex26

  Il y a 2 manières d'insérer un espace :
   - En attendant simplement (voir (idling) et (space) dans le code).
   - En entrant le code morse <AS> (.-...; attendre une seconde). Ce choix est plus judicieux pour parler à un ordinateur.

  Retour à la ligne est généré par le code morse <KN> (-.--.)
  Backspace est généré par le code morse d'erreur (...... et .......).
  Seul 6 ou 7 points sont reconnus comme caractère d'erreur (la longueur maximale d'un caractère étant 7 bits).

  Le haut-parleur est relié à D0.
  Le keyer est relié à D1.
  Le sélecteur de vitesse est relié à D2 (60 ou 90 caractères par minute).
  Le sélecteur de mode d'espace est relié à A0. A0 est le pin de reset.

  ##########################################################################################################################*/



// Déclaration de la librairie clavier du Digispark
#include <DigiKeyboard.h>

// Déclaration de la manière dont l'ordinateur reconnait le Digispark
#define QWERTY
//#define AZERTY

// Déclaration du fichier contenant la table de conversion morse -> caractères
#ifdef QWERTY
#include "MorseToChar_EN.h"
#endif
#ifdef AZERTY
#include "MorseToChar_FR.h"
#endif

// Déclaration des différentes variables
unsigned char key;
unsigned char charactercode;
unsigned int idling;
unsigned int timepressed;
unsigned int timenotpressed;
unsigned char morsechar;
unsigned char inconnu = 0;

// Déclaration des constantes de temps
unsigned int dithalf;
unsigned int maxdit;
unsigned int maxchar;
unsigned int space;

// Déclaration des pins
#define Buzzer 0
#define Keyer 1
#define SpeedSwitch 2
#define SpaceSwitch 0  // L'entrée analogique 0 (A0) représente le pin de reset
#define Freq 700

void setup() {
  tone (Buzzer, Freq);
  pinMode(Buzzer, OUTPUT);
  // pinMode(Keyer, INPUT_PULLUP);
  // pinMode(Speedswitch, INPUT_PULLUP);
  // Cela ne fonctionne pas, le compilateur retourne une erreur, "INPUT_PULLUP was not declarded in this scope", WTF ?
  // Donc on utilise la notation C : (attention si vous désirez modifier les pins sur lesquels sont reliés les boutons)
  PORTB |= ((1 << PORTB1) | ( PORTB2));

  // Initialise le clavier avec l'ordinateur (nécessaire avec certains ordinateurs)
  DigiKeyboard.sendKeyStroke(0);
}

void printdisplay(void) {
  // Convertit la variable en caractère
  morsechar = morsetable[charactercode];
  // Si seulement il s'agit d'un caractère reconnu, alors l'envoyer à l'ordinateur
  if (morsechar != inconnu) {
    DigiKeyboard.write(morsechar);
  }
}

void getkey(void) {
  // Cette fonction récupère l'état du keyer et gère le retour sonore
  key = !( digitalRead(Keyer) );  // Si le keyer est appuré, key = 1
  if (key == 1) {
    pinMode(Buzzer, OUTPUT);  // Si le keyer est appuyé, produire une tonalité
  }
  else {
    pinMode(Buzzer, INPUT);  // Sinon arrêter la tonalité
  }
}

void getkeytimes(void) {
  // Initialisation des variables de temps
  timepressed = 0;
  timenotpressed = 0;
  idling = 0;
  // Deux cas de figures se présentent ici, soit la fois précédente la condition de sortie était :
  //    - (timenotpressed >= maxchar), alors dans ce cas, le keyer n'est pas pressé, donc idling va être égal à la durée entre les 2 appuis sur le keyer auquel on soustrait (maxchar)
  //    - key == 1, alors dans ce cas, le keyer est pressé et idling = 1 (car il exécute une fois la séquence avant de vérifier la condition (key == 0)
  do {
    getkey();
    DigiKeyboard.delay(1);
    idling++;
  }
  while (key == 0);
  // Mesure la durée durant laquelle le keyer est appuyé
  do {
    DigiKeyboard.delay(1);
    timepressed++;
    getkey();
  }
  while (key == 1);
  // Mesure la durée durant laquelle le keyer est relâché
  // S'arrête si jamais cette durée dépasse (maxchar)
  do {
    DigiKeyboard.delay(1);
    timenotpressed++;
    getkey();
  }
  while ((key == 0) & (timenotpressed < maxchar));
}

void loop() {
  // Vous pouvez ici modifier la vitesse de frappe
  // Plus dithalf sera long, plus la vitesse de frappe sera lente
  if (digitalRead(SpeedSwitch) == LOW) { // Si SpeedSwitch est à la masse, vitesse rapide sinon vitesse lente
    dithalf = 33;   // Durée d'une moitié de dit en ms; vitesse de 90 caractères/min
  }
  else {
    dithalf = 90;  // Durée d'une moitié de dit en ms; vitesse de 60 caractères/min
  }

  // Génération de toutes les constantes de temps en fonction de dithalf
  maxdit = dithalf * 3;  // 1.5 * dit; durée maximum pour un dit
  maxchar = dithalf * 4;  // 2 * dit; durée max relachement entre 2 caractères
  space = dithalf * 6;   // 3 * dit; durée entre les mots
  // Un dit doit faire moins de (maxdit)
  // Un dah doit faire plus de (maxdit)
  // Si la durée entre deux appuis est supérieure à (maxchar), alors on envoi le caractère actuel et on passe au caractère suivant
  // Si de plus la durée est supérieure à (maxchar + space) alors si l'utilisateur l'a configuré, on insère un espace

  charactercode = 1;

  // Tant qu'entre deux appuis sur le keyer la durée non appuyée est inférieure à (maxchar), on récupère le caractère
  // Dès que cette durée est dépassée, on envoi le caractère et on passe au caractère suivant
  do {
    getkeytimes();  // Récupère toutes les constantes de temps

    // Si SpaceSwitch est mit à la masse et (idling) dépasse la durée minimale d'un espace alors on envoit un espace
    // Sinon on continu de décoder le caractère normalement
    if ((idling > space) & (analogRead(SpaceSwitch) < 800)) {
      printdisplay();
    }

    // On décalle charactercode d'un bit vers la gauche
    charactercode = charactercode << 1;
    // Si la durée d'appui dépasse la durée max d'un dit, alors on considère qu'il s'agit d'un dah et on met un 1 au LSB de charactercode
    // Sinon on laisse le 0 qui est mit par défaut au LSB
    if (timepressed > maxdit) {
      charactercode++;
    }
  }
  while (timenotpressed < maxchar);
  printdisplay();
}

